import RatingDetails from './RatingDetails/RatingDetails';
import classes from './RatingsList.module.css';

export default function RatingsList(props) {
  return (
    props.entries.length !== 0 && (
      <div className={classes.ratingsList}>
        {props.entries.map((entry) => (
          <RatingDetails {...entry} />
        ))}
      </div>
    )
  );
}
