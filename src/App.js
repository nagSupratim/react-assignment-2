import { useState } from 'react';
import FeedbackForm from './Components/FeedbackForm/FeedbackForm';
import RatingsList from './Components/RatingsList/RatingsList';

function App() {
  const [allEmployeeFeedbacks, setAllEmployeeFeedbacks] = useState([]);
  const onNewEmployeeAdd = (entry) => {
    setAllEmployeeFeedbacks((prev) => [entry, ...prev]);
  };
  return (
    <>
      <FeedbackForm onNewEntry={onNewEmployeeAdd} />
      <RatingsList entries={allEmployeeFeedbacks} />
    </>
  );
}

export default App;
